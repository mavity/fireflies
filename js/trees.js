function loadTrees (scene) {
    deciduous(scene);
    conifer(scene);
}

function deciduous(scene){
    gltfLoader.load('models/laurel_tree_-_low_poly/scene.gltf', (gltf) => {
        gltf.scene.scale.set(1 / 50, 1 / 50, 1 / 50);
        gltf.scene.position.z = -5;
        gltf.scene.position.x = 5;
        gltf.scene.position.y = 0.4;
        gltf.scene.traverse(function (object) {
            if (!object.isMesh) {
                return;
            }
            object.castShadow = true;
            object.receiveShadow = true;
        });
        scene.add(gltf.scene);
    });
}

function conifer(scene){
    const positions = [
        new THREE.Vector3(10, -0.2, 10),
        new THREE.Vector3(5, -0.2, 11),
        new THREE.Vector3(-7, -0.2, -4),
    ];
    const scales = [1, 2 / 3, 1.2];

    gltfLoader.load('models/spruce_tree_-_low_poly/scene.gltf', (gltf) => {

        const mesh = gltf.scene.getObjectByName('tree_tree-spruce_0');
        const geometry = new THREE.InstancedBufferGeometry();
        THREE.BufferGeometry.prototype.copy.call(geometry, mesh.geometry);

        const trees = new THREE.InstancedMesh(geometry, mesh.material, positions.length);
        trees.instanceMatrix.setUsage(THREE.DynamicDrawUsage);
        const dummy = new THREE.Object3D();
        for (let i = 0; i < positions.length; ++i) {
            dummy.position.set(
                positions[i].x,
                positions[i].y,
                positions[i].z);
            dummy.scale.set(
                scales[i],
                scales[i],
                scales[i]);
            dummy.rotation.x = -Math.PI / 2;
            dummy.updateMatrix();
            trees.setMatrixAt(i, dummy.matrix);
        }
        trees.traverse(function (object) {
            if (!object.isMesh) return;
            object.castShadow = true;
            object.receiveShadow = true;
        });
        scene.add(trees);
    });
}

