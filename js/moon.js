function getMoon() {
    const texture = new THREE.TextureLoader().load('img/moon.jpeg');
    const material = new THREE.MeshLambertMaterial({map: texture, opacity: 0});
    const geometry = new THREE.SphereGeometry(1, 20, 40);
    const moon = new THREE.Mesh(geometry, material);
    moon.position.set(10, 30, 1);
    return moon;

}

function getMoonLight() {
    const light = new THREE.DirectionalLight(0xffffff, 1.2, 100);
    light.position.set(5, 30, 1);
    light.castShadow = true;

    light.shadow.camera.top = 500;
    light.shadow.camera.bottom = -500;
    light.shadow.camera.left = -500;
    light.shadow.camera.right = 500;
    light.shadow.camera.near = 0.1;
    light.shadow.camera.far = 8000;

    return light;
}