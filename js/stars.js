function loadStars(startsCount) {
    const vertices = [];
    const initColors = [0xffffff, 0xFDB4AF, 0xffffff, 0xFDFDAF, 0xAFFDFC, 0xE6AFFD, 0xBBFDAF, 0xffffff];
    const colors = [];

    for (let i = 0; i < startsCount; i++) {
        const r = Math.random() * 10 + 40;
        const u = Math.random();
        const v = Math.random();
        const theta = 2 * Math.PI * u;
        const phi = Math.acos(2 * v - 1);
        const x = (r * Math.sin(phi) * Math.cos(theta));
        let y = (r * Math.sin(phi) * Math.sin(theta));
        const z = (r * Math.cos(phi));
        vertices.push(new THREE.Vector3(x, y, z));
        colors.push(new THREE.Color(initColors[Math.floor(Math.random() * Math.floor((initColors.length)))]));
    }

    const geometry = new THREE.Geometry();
    geometry.vertices.push(...vertices);
    geometry.colors.push(...colors);
    const material = new THREE.PointsMaterial({size: 0.05, vertexColors: THREE.VertexColors});
    return new THREE.Points(geometry, material);
}


function starsAnimation(stars) {
    const initColors = [0xffffff, 0xFDB4AF, 0xffffff, 0xFDFDAF, 0xAFFDFC, 0xE6AFFD, 0xBBFDA, 0xffffff];
    for (let i = 0; i < startsCount; i += 50) {
        stars.geometry.colors[Math.floor(Math.random() * Math.floor((startsCount)))]
            = new THREE.Color(initColors[Math.floor(Math.random() * Math.floor((initColors.length)))]);
    }
    stars.geometry.colorsNeedUpdate = true;
}