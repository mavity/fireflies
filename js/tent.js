const loadTent = function (scene) {
    gltfLoader.load('models/tent_bmt_00/scene.gltf', (gltf) => {
        gltf.scene.scale.set(1.5, 1.5, 1.5);
        gltf.scene.position.z = 30;
        gltf.scene.position.x = -200;
        gltf.scene.position.y = 0;
        // gltf.scene.rotation.y = 10;
        gltf.scene.traverse(function (object) {
            if (!object.isMesh) {
                return;
            }
            object.castShadow = true;
            object.receiveShadow = true;
        });
        scene.add(gltf.scene);
    });
}