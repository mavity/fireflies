 function loadFire (scene) {
    logs(scene);
    return fire(scene);
}

 function fireAnimation(fireLight, fireMixer) {
     if (typeof fireAnimation.counter == 'undefined') {
         fireAnimation.counter = 0;
     }
     if (fireMixer)
         fireMixer.update(0.01);
     if (fireLight && fireAnimation.counter % 8 === 0)
         fireLight.intensity = Math.random() + 0.8;
     fireAnimation.counter++;
 }

function logs  (scene) {
    gltfLoader.load('models/campfire_logs/scene.gltf', (gltf) => {
        gltf.scene.scale.set(0.009, 0.009, 0.009);
        gltf.scene.position.z = 5;
        gltf.scene.position.x = 2;
        gltf.scene.traverse(function (object) {
            if (!object.isMesh) {
                return;
            }
            object.castShadow = true;
            object.receiveShadow = true;
        });
        scene.add(gltf.scene);
    });
}

function fire  (scene) {
    gltfLoader.load('models/fire_animation/scene.gltf', (gltf) => {
        gltf.scene.scale.set(0.2, 0.1, 0.2);
        gltf.scene.position.z = 5;
        gltf.scene.position.x = 2;
        gltf.scene.position.y = 1;
        gltf.scene.traverse(function (object) {
            if (!object.isMesh) {
                return;
            }
            object.castShadow = true;
            object.receiveShadow = true;
        });

        // animate fire
        fireMixer = new THREE.AnimationMixer(gltf.scene);
        gltf.animations.forEach((clip) => {
            fireMixer.clipAction(clip).play();
        });
        scene.add(gltf.scene);

        // light
        fireLight = new THREE.PointLight(0xF79300, 3.4, 20, 1);
        fireLight.position.set(2, 1.5, 5);
        scene.add(fireLight);
        return fireLight;
    });
}
