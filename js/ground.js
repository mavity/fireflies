function getGround(color) {
    const material = new THREE.MeshStandardMaterial({color: color, side: THREE.DoubleSide});
    const geometry = new THREE.PlaneGeometry(30, 30);
    const ground = new THREE.Mesh(geometry, material);
    ground.rotation.x = -Math.PI / 2;
    ground.receiveShadow = true;

    return ground;
}