function loadFlies(fliesCount, flies, fliesLights, scene) {
    const colors = [0xEDD400, 0xBEED00, 0xECFBAC, 0xFFBD1F];
    for (let i = 0; i < fliesCount; i++) {
        const color = colors[Math.floor(Math.random() * Math.floor((colors.length)))];
        const geometry = new THREE.SphereGeometry(0.01, 32, 32)
        const material = new THREE.MeshBasicMaterial({color: color});
        let sphere = new THREE.Mesh(geometry, material)

        const x = Math.random() * 8 - 4;
        const y = Math.random() + 0.5;
        const z = Math.random() * 5 - 2;

        sphere.position.set(x, y, z);
        sphere.scale.x = sphere.scale.y = 2;

        scene.add(sphere);
        flies.push(sphere);

        let light = new THREE.PointLight(color, 1, 2, 2);
        light.position.set(x + 0.1, y + 0.1, z + 0.1);

        scene.add(light);
        fliesLights.push(light);
    }
}

function fliesAnimation() {
    if (typeof fliesAnimation.counter == 'undefined') {
        fliesAnimation.counter = 0;
    }
    if (typeof fliesAnimation.n == 'undefined') {
        fliesAnimation.n = 0;
    }

    let i;
    if (fliesAnimation.counter % 5 === 0) {
        // 0
        const r = Math.random() * 4 + 1;
        for (i = 0; i < fliesCount; i += 5) {
            let fly = flies[i];
            let light = fliesLights[i];
            fly.position.y += Math.sin(fliesAnimation.n * 3) / 50;
            fly.position.x += Math.sin(fliesAnimation.n) / 50 * r;
            fly.position.z += Math.cos(fliesAnimation.n * 5) / 50 * r;

            light.position.y += Math.sin(fliesAnimation.n * 3) / 50;
            light.position.x += Math.sin(fliesAnimation.n) / 50 * r;
            light.position.z += Math.cos(fliesAnimation.n * 5) / 50 * r;
        }
        // 1
        for (i = 1; i < fliesCount; i += 5) {
            let fly = flies[i];
            let light = fliesLights[i];
            fly.position.z += -Math.sin(fliesAnimation.n * 3) / 40;
            fly.position.x += -Math.sin(fliesAnimation.n) / 50;
            light.position.z += -Math.sin(fliesAnimation.n * 3) / 40;
            light.position.x += -Math.sin(fliesAnimation.n) / 50;
        }
        // 2
        for (i = 2; i < fliesCount; i += 5) {
            let fly = flies[i];
            let light = fliesLights[i];
            fly.position.y += Math.cos(fliesAnimation.n * 3) / 100;
            light.position.y += Math.cos(fliesAnimation.n * 3) / 100;
        }
        // 3
        for (i = 3; i < fliesCount; i += 5) {
            let fly = flies[i];
            let light = fliesLights[i];
            fly.position.x += Math.cos(fliesAnimation.n * 3) / 50;
            light.position.x += Math.cos(fliesAnimation.n * 3) / 50;
        }
        // 4
        for (i = 4; i < fliesCount; i += 5) {
            let fly = flies[i];
            let light = fliesLights[i];
            fly.position.z -= Math.cos(fliesAnimation.n * 3) / 50;
            light.position.z -= Math.cos(fliesAnimation.n * 3) / 50;
        }
        fliesAnimation.n += 0.01;
    }
    fliesAnimation.counter++;
}